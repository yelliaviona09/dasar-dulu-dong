public class DoWhileLoop {
    public static void main(String[] args) {

        var counter = 1;

//        perbedaan do while dan while loop
//        perulangan d0 while mengecek statement atau kondisi dibelakangan sesudah mengulang statement awal,
//        sedangkan perulangan while mengecek kondisi statement di awal sebelum melakukan perulangan


        do {
            System.out.println("perulangan " + counter);
            counter++;
        }while (counter <= 10);
    }
}
