public class RecursiveMethod {
    public static void main(String[] args) {

//        menyelesaikan fqctorial dengan cara perulangan biasa

        System.out.println(factotialLoop(5));

        System.out.println(recursiveMethodFactorial(5));

        loop(100000); // menyebabkan error stackOverFlow, namun jika
        // value nya diturunkan maka tidak error tergantung kapasitas memori laptop masing-masing

    }

    static int factotialLoop(int value){
        var result = 1;

        for (var counter =1; counter <= value; counter++){
            result *= counter;
        }
        return result;

    }

//    menyelesaikan factorial menggunakan method Recursive (memanggil method nya sendiri)

    static int recursiveMethodFactorial(int value){
        if (value == 1){
            return 1;
        }else {
            return value * recursiveMethodFactorial(value - 1);
        }
    }

//    kekurangan menggunakan recursive : dapat menyebabkan error stackOverFlow
//    yang disebabkan karena terjadi perulangan terus-menerus, contoh :

    static void loop(int value){
        if (value == 0){
            System.out.println("Selesai");
        }else {
            System.out.println("Loop " + value);
            loop(value -1);
        }
    }

}
