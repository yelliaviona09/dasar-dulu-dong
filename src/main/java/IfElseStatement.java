public class IfElseStatement {
    public static void main(String[] args) {

        var nilai = 60;
        var absen = 40;

        var lulusNilai = nilai >= 78;
        var lulusAbsen = absen >= 75;

        var lulus = lulusAbsen && lulusNilai;


        if (lulus){
            System.out.println("Selamat Anda LULUS");
        }

//        agar code lebih rapih dan tidak bertele-tele, maka dapat ditulis seperti dibawah ini

        if (nilai >= 78 && absen >= 75){
            System.out.println("Selamat Anda LULUS");
        } else {
            System.out.println(" Jangan Putus Asa, coba lain Kali");
        }

        if (nilai >= 80 && absen >= 80){
            System.out.println("Nilai AKhir Anda A");
        } else if (nilai >= 70 && absen >= 70) {
            System.out.println("Nilai Akhir Anda B");
        } else if (nilai >= 60 && absen >= 60) {
            System.out.println("Nilai Akhir Anda C");
        } else if (nilai >= 50 && absen >= 50) {
            System.out.println("Nilai Akhir Anda D");
        } else {
            System.out.println("Nilai Akhir Anda E, silahkan mengulang di semester depan");
        }
    }
}
