public class Komentar {
    public static void main(String[] args) {

        System.out.println(sum(10,10));


    }

    /**
     * ini contoh komentar multiline
     * method untuk menjumlahkan value 1 dan value 2
     * @param value1 angka pertama
     * @param value2 angka kedua
     * @return hasil penjumlahan komentar 1 dan value 2
     */

    static int sum(int value1, int value2) {
//        jumlahkan kedua value (ini komentar single line)
        return value1 + value2;
    }
}
