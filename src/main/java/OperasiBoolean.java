public class OperasiBoolean {
    public static void main(String[] args) {

        var nilaiUjian = 80;
        var nilaiAkhir = 70;

        var lulusUjian = nilaiUjian >= 78;
        var lulusNilaiAkhir =  nilaiAkhir >= 78;
        var lulus = lulusUjian && lulusNilaiAkhir;

        System.out.println(lulus);

    }
}
