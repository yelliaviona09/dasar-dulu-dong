public class TipeDataBukanPrimitif {
    public static void main(String[] args) {

        Integer iniInteger = 100;
        Long iniLong = 100000L;
        Byte iniByte =null;

        System.out.println(iniByte);

        iniByte = 100;
        System.out.println(iniByte);


//         konversi data primitif ke non primitif

        int iniInt = 24_09_1998;
        Integer iniInteger2 = iniInt;

        System.out.println(iniInteger2);


//        konversi dari tipe data primitif ke non primitif tapi bukan value yang compatible
//        misal short dan byte (dalam tipe data non primitif dia punya method, memanggil method seperti

        Integer iniObject = iniInt;
        short iniShort = iniObject.shortValue();
        long iniLong2 = iniObject.longValue();
        float iniFloat = iniObject.floatValue();


    }
}
