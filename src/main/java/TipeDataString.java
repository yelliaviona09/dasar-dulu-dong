import javax.sound.midi.Soundbank;

public class TipeDataString {
    public static void main(String[] args) {

        String first = "Welcome to";
        String second = "Viona's World";
//        menggabungkan string
        String full = first + " " + second;

//        System.out.println(first + " " + second);

//        atau menggunakan cara
        System.out.println(full);
//        System.out.println(first);
//        System.out.println(second);
    }
}
