public class TipeDataChar {
    public static void main(String[] args) {

        char v = 'V';
        char i = 'I';
        char o = 'O';
        char n = 'N';
        char a = 'A';


//        System.out.println(v + "" + i + "" + o + "" + n + "" + a); // menggunakan println tetapi ingin memunculkan tanpa enter

//        berikut jika ingin print tanpa line satu-persatu
        System.out.print(v);
        System.out.print(i);
        System.out.print(o);
        System.out.print(n);
        System.out.print(a);
    }
}
