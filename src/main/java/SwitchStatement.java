public class SwitchStatement {
    public static void main(String[] args) {

        var nilai = "A";

//        cara yang biasa digunakan menggunakan if
//        if (nilai == "A"){
//            System.out.println(lulus);
//        }

//        diubah menggunakan switch

        switch (nilai) {
            case "A":
                System.out.println("Anda Lulus Dengan Sangat Baik");
                break;

            case "B":
            case "C":
                System.out.println("Nilai Anda Cukup Baik");
                break;
            case "D":
                System.out.println("Anda Tidak Lulus");
                break;
            default:
                System.out.println("Sepertinya Anda Salah Jurusan");

        }

//        untuk java versi 14 keatas dapat menggunakan switch lambda, berikut

        switch (nilai){
            case "A" -> System.out.println("Anda Lulus Dengan Sangat Baik");
            case "B", "C" -> System.out.println("Nilai Anda Cukup");
            case "D" -> System.out.println("Anda tidak Lulus");
            default -> {
                System.out.println("Sepertinya Anda Salah Jurusan");
            }
        }

// tanpa penggunaan yield (tetapi akan banyak duplicate code

        String ucapan;
        switch (nilai){
            case "A" -> ucapan = "Anda Lulus Dengan Sangat Baik";
            case "B", "C" -> ucapan = "Nilai Anda Cukup";
            case "D" -> ucapan = "Anda tidak Lulus";
            default -> {
                ucapan = "Sepertinya Anda Salah Jurusan";
            }
        }
        System.out.println(ucapan);

//        contoh menggunakan yield keyword (mirip dengan return value karena berisi balikan)

        ucapan = switch (nilai) {
            case "A":
                yield  "Anda Lulus Dengan Sangat baik";
            case "B", "C":
                yield  "Nilai Anda Cukup";
            case "D":
                yield  "Anda tidak Lulus";
            default:
                yield  "Sepertinya Anda Salah Jurusan";
        };
                System.out.println(ucapan);

    }
}
