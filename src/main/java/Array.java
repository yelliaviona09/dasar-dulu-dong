public class Array {
    public static void main(String[] args) {

        String[] cobainArray;
        cobainArray = new String[3];

        cobainArray[0] = "Yellia";
        cobainArray[1] = "Viona";
        cobainArray[2] = "Anugerah";
        System.out.println(cobainArray[0] + " " + cobainArray[1] + " " + cobainArray[2]);


//        array initializer (tidak perlu menentukan jumlah array
//        karena ketika memasukkan index array akan otomatis terhitung, contoh

        String[] nama = {
                "Yellia", "Viona"
        };
        //        mengganti indeks array ke 0 menjadi bernilai null

        nama[0] = null;

        int[] arrayInt = new int[]{
            1,2,3,4,5,6,7,8,9,10
        };

        long[] arrayLong = new long[]{
                10L, 20L, 30L, 40L, 50L, 60L
        };

        System.out.println(arrayLong.length);

//        mengganti indeks array ke 0 menjadi bernilai 0
        arrayLong[0] =0;

//        array didalam array

        String[][] arrayDidalamArray ={
                {"Yellia", "Viona"},
                {"Anugerah","Nugraha"},
                {"Nadia", "Rozzana"}
        };

//        misal ingin mengambil indeks "viona"

        System.out.println(arrayDidalamArray[0][1]);

        //        misal ingin mengambil indeks "anugerah"

        System.out.println(arrayDidalamArray[1][0]);

    }
}
