public class OperasiMatematika {
    public static void main(String[] args) {

        int a = 100;
        int b = 25;

        System.out.println(a + b);
        System.out.println(a - b);
        System.out.println(a * b);
        System.out.println(a / b);
        System.out.println(a % b);


//        augmented assignment

        int c = 100;

        c += 10;
        System.out.println(c);

        c -= 10;
        System.out.println(c);

        c *= 10;
        System.out.println(c);


//        unary operator

        int d = 100;
        int e = -25;

        d++;
        System.out.println(d);

        d--;
        System.out.println(d);

        System.out.println(!true);
    }
}
