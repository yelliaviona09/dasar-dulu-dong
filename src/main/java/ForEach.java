public class ForEach {
    public static void main(String[] args) {

        String[] names = {
                "Yellia", "Viona", "Belajar",
                "Java", "Fundamemtal"
        };

//        menggunakan for biasa, agak jelimet untuk codenya

        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

        // menggunakan for each
        System.out.println("For each");

        for (var name: names){
            System.out.println(name);
        }
    }
}
