public class MethodVariabelArgument {
    public static void main(String[] args) {


//        berikut contoh jika menggunakan array

        int[] values = {80,50,75,60,75};
        congrats("Viona", values);

        congrats("viona", 80,80,80,99,40);
    }

//    static void congrats(String name, int[] values){
//        var total = 0;
//        for (var value : values){
//            total += value;
//        }
//        var finalValue = total / values.length;
//
//        if (finalValue >= 75){
//            System.out.println("Selamat " + name + ", kamu lulus");
//        } else {
//            System.out.println("maaf " + name + ", kamu belum lulus");
//        }


//        jika menggunakan variabel argumen

        static void congrats(String name, int... values){
            var total = 0;
            for (var value : values){
                total += value;
            }
            var finalValue = total / values.length;

            if (finalValue >= 75){
                System.out.println("Selamat " + name + ", kamu lulus");
            } else {
                System.out.println("maaf " + name + ", kamu belum lulus");
            }

    }


}
