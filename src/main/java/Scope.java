public class Scope {
    public static void main(String[] args) {

        sayHello("Viona");

    }

    static void sayHello(String name){
        String msg = "Hello " + name;

        if (!name.isBlank()) {
            String hi = "Hello " + name;
            System.out.println(hi);
        }

        System.out.println(msg);
//        System.out.println(hi); // ini akan error karena hi terletak di scop if, atau dalam bloc say hello

    }
}
