public class MethodParameter {
    public static void main(String[] args) {
        sayHello("Yellia", "Vionaa");
        sayHello("Kaila", "dahda");
        sayHello("Ramadani", "safitra");
    }

    static void sayHello(String firstName, String lastName){
        System.out.println("Hello " + firstName + " " + lastName);
    }
}
