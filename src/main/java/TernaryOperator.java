public class TernaryOperator {
    public static void main(String[] args) {

//        code menggunakan ternary operator akan menjadi lebih sederhana
        var nilai = 75;
        String ucapan = nilai >= 75 ? "Selamat Anda Lulus" : "Coba Lagi dilain Kesempatan";

        System.out.println(ucapan);



// code tanpa ternary operator
        if (nilai >= 75){
            ucapan = "Selamat Anda Lulus";
        } else {
            ucapan = "coba lagi dilain kesempatan";
        }
        System.out.println(ucapan);
    }
}
